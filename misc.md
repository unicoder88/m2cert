# uiElement - ui/ ... /lib/core/element/element.js
## initLinks:
- imports
- exports
- links == same value in imports and exports

imports: {
    totalRecords: '${ $.provider }:data.totalRecords'
}
same as
imports: {
    totalRecords: 'example.example_data_source:data.totalRecords'
}

exports: {'visible': '${ $.provider }:visibility'}
imports: {'visible': '${ $.provider }:visibility'}
links: {'visible': '${ $.provider }:visibility'}
links: {value: '${ $.provider }:${ $.dataScope }'}

listens: {'${ $.namespace }.${ $.namespace }:responseData': 'setParsed'}
listens: {'${ $.provider }:data.overload': 'overload reset validate'} -- call multiple callbacks
listens:
  'index=create_category:responseData' => 'setParsed',
  'newOption' => 'toggleOptionSelected'

$.someProperty - property of the UI component in the scope of this component

conditions in string literals:
'${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate'


<argument name="data" xsi:type="array">
    <item name="config" xsi:type="array">
        <item name="exports" xsi:type="array">
            <item name="visible" xsi:type="string">sample_config.sample_provider:visibility</item>
        </item>
    </item>
</argument>

## initObservable
observe([{Boolean} isTracked,] {String|Array|Object} listOfProperties)
observe('var1 var2 var3')
observe(['var1', 'var2', 'var3'])
observe(true, 'var1 var2 var3') -- isTracked, use property accessors
observe(false, 'var1 var2 var3') -- not tracked, use observable properties
observe({var1: 'value', var2: true}) -- initial values from object

## initModules
defaults: {
    modules: {
        '%myProperty%': '%linkToTheComponent%',
        externalSource: '${ $.externalProvider }',
        street: '${ $.parentName }.street',
        city: '${ $.parentName }.city',
        country: '${ $.parentName }.country_id',
        productForm: 'product_form.product_form'
    }
}
this.productForm().reload()

# uiCollection - ui/ ... /lib/core/collection.js
initElement(childInstance)
destroy() - delete from uiRegistry, listeners, link in parent
getChild(index)
.elems() -- observable
.childDefaults (!)


data-bind="%binding_name%: %value%"
%binding_alias%="%value%"
<%binding_alias% args="%value%">

<form>
  <argument name="data" xsi:type="array">
      <item name="js_config" xsi:type="array">
          <item name="provider" xsi:type="string">category_form.category_form_data_source</item>
          <item name="deps" xsi:type="string">category_form.category_form_data_source</item>
      </item>

<item name="config" xsi:type="array">
    <item name="provider" xsi:type="string">[ComponentName].[ComponentName]_data_source</item>
</item>

  </argument>
</form>

${ $.provider }:data.totalRecords
[ComponentName].[ComponentName]_data_source

reinitialize x-init scripts:
require('mage/apply/main').apply();

page-cache
<!-- BLOCK .+ -->
<!-- /BLOCK .+ -->

KO Bindings
-----------
afterRender <div id="checkout-agreements-modal" data-bind="afterRender: initModal" style="display: none">

Data-mage-init
--------------
accordion
actionLink
addToCart
baseImage
captcha
catalogGallery
checkoutLoader
collapsible
compareList
confirmRedirect
details
discountCode
downloadable
dropdown
dropdownDialog
floatingHeader
form
giftOptions
globalNavigation
globalSearch
groupedProduct
loader
loaderAjax
menu
multiShipping
openVideoModal
orderOverview
ordersReturns
passwordStrengthIndicator
popupWindow
popupWindow
priceOptionDate
priceOptionFile
productSummary
quickSearch
recentlyViewedProducts
redirectUrl
shoppingCart
sticky
tabs
taxToggle
toggleAdvanced
toolbarEntry
tooltip
translateInline
upsellProducts
validation
wishlist

links: {
    value: '${ $.provider }:${ $.dataScope }'
}

Magento Fundementals
====================
2.1.0
Deadline Feb 02
Unit 1 Preparation & Configuration + Quiz
Unit 2 Request Flow - bootstrap, application types, dispatch, routers, controller, rewrite
Unit 3 Rendering - layouts, UI components, view elements (container, ui_component, block etc.)
Unit 4 Databases & EAV - adapter, select, collections, flat vs eav, setup, model, attributes
Unit 5 Service Contracts - API, seach criteria, repository
Unit 6 AdminHTML - ACL, system.xml, menu, grids, forms, data source, ui components, grid indexer

area: global, frontend, admin, adminhtml, crontab, webapi_rest, webapi_soap, doc
events.xml, routers.xml, acl.xml, etc
sequence - load module - optional?
app/i18n/
app/design/frontend/Magencto/luma

cli commands:
Magento\Framework\Console\CommandListInterface.commands[]

## environment variables
MAGE_MODE - developer   /default/    production
MAGE_REQUIRE_MAINTENANCE

## constants:
BP = /
VENDOR_PATH

## config files:
/magento_umask - default 002
/app/etc/vendor_path.php

## app/bootstra.php: -> autoload.php, functions.php
error_reporting

## modes: default, developer, production. MAGE-MODE env variable
Developer:
- exceptions displayed, not logged
- exception thrown if bad event subscr.
- var/report detailed
- static created dynamically - symlinked???, changes visible immediately

Production:
- errors logged, not displayed
- static not created dynamically, must be deployed
- not need for www-data to write, pub/static can be read-only

Default:
- errors logged in var/report, not displayed
- static created dynamically - copied! changes not visible

## Maintenances
var/.maintenance.flag
var/.maintenance.ip
$bootstrap->run($app)
  assertMaintenance
  $_SERVER[MAGE_REQUIRE_MAINTENANCE] - can disable check or force enabled

## Object Manager
get - singleton
create - new instance

Interceptor - uses trait with ___init
app/etc/di.xml
<type name="">
  <argument name="..." xsi:type="object" shared="false">..</argument>
  <plugin name="" type="" sortOrder="" disabled="" />
plugin can be used for virtual type, interface

beforeMethod($subject, $arg1, ...) -- modify arguments. return [$arg1, ...]
afterMethod($subject, $result) -- modify return value. return $result
aroundMethod($subject, $proceed, $arg1, ...)

## Config Order
app/etc/config.php
app/etc/di.xml
di.xml
... event.xml ...
area-specific/

Magento\Framework\Config
  DataInterface - retrieve config data within a scope
  ScopeInterface - current scope, scope's data
  FileResolverInterface - set of files to read by reader
  ReaderInterface

## Adding own config type
xml
xsd before merge
xsd merged
config class - public methods that will return your data for your code
config reader - specify what files to read and where
schema locator - path to xsd and xsd merged
converter - xml to array

Unit 2
======
front countroller, loop, dispatch
register router, router routes
controller dispatch
response

frontController.routerList.di($routerList) - standard, urlrewrite, cms, default
App\Router\Base._routeConfig._routes = from reader + cache

## bootstrap.run(app)
errorHandler
maintenance
installed - deploymentConfig.isAvailable
app.launch
- are code by front name
- configure object manager by area
- front controller (front controller, soap, rest)
- front controller.dispatch
- result either
    - \Magento\Framework\Controller\ResultInterface - renderResult into response
      headers, render, events `layout_render_before`, `layout_render_before_{$fullActionName}`
    - \Magento\Framework\App\Response\HttpInterface - use returned response. deprecated
- event `controller_front_send_response_before`

## routers
frontend:
- standard (20)
-   *dynaic_robots_router* (22)
-   *amasty_seo_router* (22)
-   *amasty_shopby_root* (35)
- urlrewrite (40)
- cms (60)
- default
adminhtml:
- admin (10) - extends base
- default (100)

## frontController
App\FrontController, Webapi\Controller\Rest, Webapi\Controller\Soap
dispatch:
- routerList:
  - standard - frontName/controller/action
    - parse front/controller/action
    - match by registered modules => modules[]
      - scope = frontend
      - routes.xml, frontend, routes.xsd, routes_merged.xsd. config.router[].route[]
    - check action found in actionList - cached list of all controllers
    - null - frontName not registered, frontName.noroute or action
  - urlrewrite
    - handle ___from_store GET param
    - finds rewrite
    - redirects if needed by redirect_type
    - forwards to target
  - cms - load from DB by request URI
    - event `cms_controller_router_match_before`, can redirect or abort match
  - default - noRouterHandlerList[].process (class, sortOrder), always returns forward
    - 'default' handler
       explode config(web/default/no_route) -- module/controller/action
       default cms/noroute/index --> CMS Page = config(web/default/cms_no_route), fallback cms/index/defaultNoRoute if cms page not found
       fallback core/index/index - not registered, won't match
    - 'backend' - adminhtml/noroute/index
- router.match - return controller action or null
- if dispatched && match-break

check not dispatched, match, dispatched(true), execute (can set undispatched), goto beginning

### noroute
return forwardResult. {matched, dispatched=false, action=noroute}
restart routers loop, catalog/product/noroute
all routers not match, default matches, page from config

## actions
App\ActionInterface
App\Action\AbstractAction - request and response from context
App\Action\Action - dispatch implementation, _forward, _redirect, session namespace
Backend\App\Action\AbstractAction
Backend\App\Action\Action - _isAllowed, ensure logged in, set default locale, content helpers

## action.dispatch or action.execute
dispatch plugins:
- httpContext - context_store, context_currency, context_group - customer group, context_auth - isLoggedIn
- design - designLoader->load - design, translate
events `controller_action_predispatch`, `controller_action_predispatch_{$route}`, `controller_action_predispatch_{$fullAction}`
execute (unless flagged or dispatched)
events `controller_action_postdispatch_{$fullAction}`, `controller_action_postdispatch_{$route}`, `controller_action_postdispatch` (unless flagged)

!pub/static/ --- version.* => pub/static.php?resource=$0

## response
App\Console\Response
App\Response\Http
  File\Storage\Response
Webapi\Response
  Webapi\Rest\Response

Zend\Stdlib\Message - metadata[], content
Zend\Http\AbstractMessage - version, headers
Zend\Http\Response - status code, decode
Zend\Http\PhpEnvironment\Response - send headers, content once
Magento\Framework\HTTP\PhpEnvironment\Response - some helpers
Magento\Framework\App\Response\Http - magento-specific - DI, context, cache headers - TTL public/private, nocache

response.representJson(value)

## rendering
ResultInterface:
- Layout - events `layout_render_before`, `layout_render_before_{$actionName}`; addHandle, addUpdate; 'module_controller_action' handle
  - Page - 'default' handle, template, vars requireJs, head*, body*, html*, loaderIcon
- Json (setData, setJsonData)
- Redirect - setUrl, setPath, setRefererUrl
- Forward - dispatched(false); forward, set{Module,Controller,Params}
- Raw (setContents)


Unit 3
======
page.render -> pageConfig.publicBuild = build -> View\Layout\Builder.build:
- loadLayoutUpdates - load all layouts, filter only current handles + add dynamic updates
- generateLayoutXml - stupid, wrap filtered xml as object
- generateLayoutBlocks - create blocks tree

## layout builder:
- events `layout_load_before`, `layout_generate_blocks_before`


layout hardcoded switch: ui component/block/container
_renderContainer - id, class, tag

View\Element\BlockInterface.toHtml
_prepareLayout

View\Element\Text, TextList, Template, Messages, Redirect
template: setTemplate, constructor argument in $data

templateContext = $this, setTemplateContext
templateEngine->render(templateContext=this=$block, $filename, $viewVars)

##controller._view -- controller action stuff like in M1
- getPage, getLayout
- loadLayout(handles, ...)
- addActionLayoutHandles, addPageLayoutHandles
- generateLayoutXml, generateLayoutBlocks
- renderLayout
- events `controller_action_layout_render_before`, `controller_action_layout_render_before_{$fullActionName}`

##layout
builder.generateLayoutBlocks -> layout.generateElements
readerPool.*interpret* -- schedules
  nodeReaders[type].interpret each element -- Layout\ReaderInterface
  - html, move
  - body - own readerPool.interpret, but without 'body' reader
  - head - css,script,link,remove,meta,title,attribute
  - 'container', 'referenceContainer' --> Layout\Reader\Container
  - 'block', 'referenceBlock' --> Layout\Reader\Block
  - uiComponent
  View\Layout\ReaderInterface::interpret -- html, body, head, ui component, reader pool, container, move, block
View\Layout\GeneratorPool.process -- generates blocks
  buildStructure
  generator[].*process*
  - head, body
  - block - creates blocks, sets layout, event `core_layout_block_create_after`, block 'actions'
  - container - sets tag, id, class, label, display
  - uiComponent - creates wrapper element, prepareComponent recursively etc.
getOutput
- renderElement(root)
  - renderNonCachedElement(root) -- hardcoded switch
    - is uiComponent -> toHtml
    - is block -> toHtml
    - is container -> renderElement(child[])
  - event `core_layout_render_element`

## block template path customization:
$data['module_name'] - default from $this
$data['area'] - default from current area

## View\Element\Template\File\Resolver.getTemplateFileName
View\FileSystem.getTemplateFileName
- by default module extracted automatically by class name, e.g. this=My\Module\Block\Something => module = My_Module
- can override $data['module_name']
View\Asset\Repository.extractModule
- My_Module::template/path.phtml -> [My_Module, template/path.phtml]
- template/path.phtml -> ['', template/path.phtml]

View\Design\FileResolution\Fallback\TemplateFile.getFile -- minify html
View\Design\FileResolution\Fallback\File.getFile
View\Design\FileResolution\Fallback\Resolver\Simple.resolve
View\Design\Fallback\RulePool.getRule - by type: file, locale file, template file, 
- <theme_dir>/<module_name>/templates
- <module_dir>/view/<area>/templates
- <module_dir>/view/base/templates

## root node in layout file
<page>
- html - attributes
- body
  <attribute name value />
  <referenceContainer name remove=true />
  <ui_component />
  <block ..>
    <arguments>
      <argument name translate xsi:type>Value</argument> -- $data
  action
- head
  <css src />, <script src />, <link src ie_condition defer />, <remove src />, <title>Title</title>,
  <meta name content />, <attribute name value />
- update
<layout> - blocks and containers


View\Layout\File\Collector\Aggregated::getFiles

View\Model\Layout\Merge - fileSource.getFiles
Collector\Aggregated
- add themeFiles - ThemeModular
  - "{$namespace}_{$module}/{$this->subDir}$filePath" | decorate ModuleOutput - only enabled | decorate ModuleDependency - sort
- replace overridenBaseFiles
  - OverrideBase | decorators output, dependency
- replace themeFiles
  - Override\ThemeModular | decorators output, dependency

view/page_layout dir - same syntax as normal layout, <layout> as root node

Magento\Theme\Model\Theme.getInheritedThemes

app/design/Convert/relax/Magento_Catalog/layout/default.xml
app/design/Convert/relax/Magento_Catalog/layout/override/theme/Magento/blank/catalog_product/view.xml


Unit 3.5
========
view/<area>/requirejs-config.js

var config = {
  map: {
    '*': {
      name: 'path'
    }
  }
}
Magento_Catalog => Magento/Catalog
web/js - regular
web/js/view - ui components

plain module / jQuery widget / ui component

{
  $.widget('mage.shippingCart', {
    ... // this.config, this.element
  })
  return $.mage.shippingCart;
}
$(element).shippingCart(...)


View\Element\BlockInterface -> View\Element\UiComponentInterface
AbstractUiComponent
Module/view/<area>/ui_component/templates
Vuew\Element\UiComponent\DataSourceInterface - getDataProvider - just holds data provider config (class, params)
.xhtml - {{...}}


Ui Components
=============
\Magento\Framework\View\Layout\Generator\UiComponent::generateComponent
\Magento\Framework\View\Element\UiComponentFactory::create
\Magento\Ui\Model\Manager::prepareData
\Magento\Ui\Model\Manager::prepare


UiComponentFactory->create('product_form')
prepareData,getData - read/cache statically defined in ui_component xml
mergeMetadata -- recursively dataProvider.getMeta
                 adds [attributes][class] for each component, checks componentType

    data[arguments][data][config][componentType]
    \Magento\Framework\View\Element\UiComponent\Config\Provider\Component\Definition -- from ui_component/etc/definition.xml

each child[] -> createChildComponent -- recursive
  create([attributes][class], arguments) -- data in arguments[data], children in arguments[components]
prepareComponent - getChildComponents[]->prepare
wrap to UiComponent\Container as child 'component'
toHtml = render -> renderEngine->render (template + '.xhtml')

components described statically in ui_component/*.xml
meta arguments/data/config/componentType == node name in .xml


UNIT 4
======
ORM - object-relational mapping. class<->DB
Active Record - ORM implementation, Martin Fowler 2003. object=table + db access + domain logic. sample - Yii
  part = new Part()
  part.name = "Sample"
  part.save()

Model\AbstractModel, _construct, _init - resource
Model\ResourceModel\Db\AbstractDb, _construct, _init table_name, pk_name
Model\ResourceModel\Db\Collection\AbstractCollection, _construct, _init - model, resource
event prefix, event object
getConnection

resource->load($model, $id, $field = null)
eav resource->loadByAttribute
! don't rely on events _load_before; use plugins on resource load()
for collections: $model->afterLoad
model.getStoredData. ~ _origData
model.validateBeforeSave <- resourceModel.save
model.beforeSave, afterSave, afterCommitCallback <- resourceModel.save
model.beforeDelete, afterDelete, afterDeleteCommit


model._getValidationRulesBeforeSave
resource.getValidationRulesBeforeSave

## setup
bin/magento module:status
bin/magento setup:db:status

Setup\InstallSchema, InstallData
Setup\UpgradeSchema, UpgradeData
InstallSchemaInsterface
- startSetup, ..., endSetup
UpgradeSchemaInterface

setup.run('multiline; sql');
setup.getConnection.createTable(setup.getConnection.newTable.addColumn)

\Magento\Eav\Setup\EavSetup, \Magento\Catalog\Setup\CategorySetup
- addAttribute --- options are mapped, e.g. 'label' => 'frontend_label'
- updateAttribute --- options as is

extend and use eavSetup in Install/UpgradeData

## any EAV entity
- at least one attribute set Default
- at least one attribute group General
eav_attribute_set - per entity type, e.g. for product: Bags, Glasses etc.
eav_attribute_group - per attribute set
! eav_entity_attribute - assign attribute to set and group

## eav_entity_type:
- entity_model
- attribute_model - or empty
- increment_model - NumbericValue = last+1/Alphanum
- additional_attribute_table

## Magento\Eav\Model\Entity\AbstractEntity - load, save etc.
getAttribute, saveAttribute
getEntityTable, getWriteConnection

addAttributeToSelect
addAttributeToFilter
joinAttribute

int, varchar, text, decimal, datetime
Magento\Eav\Setup\EavSetup


Unit 5 Service Contracts
========================
data vs

Module\Api\Data - for entity
Module\Api - operational: business logic, repository

data may extend AbstractExtensibleObject
Api\AbstractSimpleObject
SimpleBuilderInterface.create

Repositories deal with Data objects, not models
- getList - SearchCriteriaInterface

CustomerRegistry

$model->getDataModel
Api\DataObjectHelper->populateWithArray

## SearchCriteriaInterface
filterGroups -> filterGroup[].getFilters[] - (field, value, conditionType)
  $collection->addFieldToSelect(['attribute' => 'name', 'or' => 'Roman'])
  - *all* groups must match - AND
  - inside group *any* filter must match - OR
  - (filter1 or filter2 or ...) AND (filter1 or filter2 or ...) AND ...
sortOrders - sortOrder[] (field, direction)
pageSize
currentPage

## AbstractSimpleObject - wraps $data in constructor, @extend
__construct(data), setData
_get, __toArray recursive
Api\Filter, Api\SortOrder, ...

## AbstractSimpleObjectBuilder = creates by class name without 'Builder' part
SearchCriteriaBuilder.addFilters().create() -> new SearchCriteria(data)
FilterBuilder.setSth().create() -> new Filter(data)

## SearchResultsInterface - items, searchCriteria, totalCount, more if needed

## AbstractExtensibleObject
  ExtensibleDataInterface (extension_attributes)
  + CustomAttributesDataInterface (custom_attributes) - {get,set}CustomAttribute(s) --- AttributeValue[]
  = AbstractExtensibleObject - _{get,set}ExtensionAttributes, getEavAttributesCodes -- from metadata

## ExtensionAttributeFactory
## AttributeValueFactory
## AttributeValue, AttributeInterface - (attributeCode, value)

extension_attributes.xml
- join support?

$model.getExtensionAttributes -> ExtensionAttributesFactory.create(class) -- by @return comment on getExtensionAttributes
  Model\Product.getExtensionAttributes(Api\Data\ProductInterface)
  Api\Data\ProductInterface.getExtensionAttributes @return Api\Data\ProductExtensionInterface
  create through 'Factory'

WebAPI
======
## webapi.xml
- route with placeholders, method GET/POST/...
- service class/method (repository)
- resources - ACL: anonymous, self, Magento ACL - admin
  <resource ref="anonymous" />

rest - url in webapi.xml, interfaces, services, methods
etc/webapi_rest/di.xml, etc/webapi_soap/di.xml

## authentication
oAuth - for customer/admin through 3rd party app? - SOAP
token - app interaction, Pimcore etc. - REST
session - admin dev

/soap?wsdl&services=catalogProductRepositoryV1
/rest/V1/integration/admin/token


Unit 6 - AdminHTML
==================
## Grid
Listing, Filters, DataSource

View\Element\UiComponent\DataProvider\DataProvider
View\Element\UiComponent\DataProvider\FilterPool, appliers = fulltext, regular

## DataProvider\AbstractDataProvider
- getData
- getCollection


Mixins
======
mage/requirejs/resolver.js
mage/requirejs/mixins.js
rjsMixins.load
applyMixins


Validation
==========

$.validation = mage.validation = vendor/magento/magento2-base/lib/web/mage/validation.js:1467
$.validate = native jQuery plugin

$.validation -- creates .validate property that proxies $.validate

country_id: {
  provider: "checkoutProvider",
  dataScope: "shippingAddress.country_id",
  config: {
    customScope: "shippingAddress"
  }
}

this.source.set('params.invalid', false);
this.source.trigger('shippingAddress.data.validate');
if (this.source.get('params.invalid') ...

js/form/element/abstract
  - '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate' -- trigered by event
  - this.validation -- rules
  - validate() -> validator(rules=this.validation, value, this.validationParams)
  - if (!isValid)
    - this.source.set('params.invalid', true);
    - this.error(message) -- ko observable for template
    - this.bubble('error', message)
lib/validation/validator
lib/validation/rules
  telephone.validation.validate-phoneLax=1234
  telephone.validationParams=5678
- 0: handler (value, params=1234, additionalParams=5678)
- 1: message


Bindings
========
Module_Ui/js/lib/knockout/bindings/bootstrap

access other bindings example:
/var/www/runandrelax/vendor/magento/magento2-base/lib/web/knockoutjs/knockout.js:3873



this.source = get(this.provider)
this.listens["event1 event2"] = "callback1 callback2 callback3"


this.source.trigger('shippingAddress.data.validate');
-> listens '${ $.provider }:${ $.customScope ? $.customScope + "." : ""}data.validate': 'validate',

## uiRegistry - Magento_Ui/js/lib/registry/registry

## uiClass - Magento_Ui/js/lib/core/class
methods:
- initialize, initConfig
props:
- contructor.defaults
config:
- ignoreTmpls

## uiElement - Magento_Ui/js/lib/core/element/element - listens/tracks, source, imports, exports, modules;
  + MERGE lib/core/events - on, off, trigger
  + MERGE lib/core/element/links - setListeners, setLinks
  methods:
  - initialize, initObservable, initModules
  properties:
  - tracks, modules, source, provider, 

## uiCollection - Magento_Ui/js/lib/core/collection
## lib/form/element/abstract - 
## uiEvents = EventsBus - js/lib/core/events - on, off, trigger

$.async - Module_UI/js/lib/view/utils/async.js

##abstract - Magento_Ui/js/form/element/abstract


Attribute Input Types
=====================
\Magento\Eav\Model\Adminhtml\System\Config\Source\Inputtype,
\Magento\Catalog\Model\Product\Attribute\Source\Inputtype,
event `adminhtml_product_attribute_types`

## Values
### eav
text
textarea
date
boolean
multiselect
select
### catalog
price
media_image
### swatches
swatch_visual
swatch_text
### weee
weee

\Magento\Catalog\Block\Adminhtml\Product\Attribute\Edit\Tab\Main::_prepareForm,
event `adminhtml_product_attribute_types`
- price
- media_image
- gallery


CRUD
====
model.load -> resource.load -> entity manager.load -> operation read.execute:
- readMain.execute
- readAttributes.execute
- readExtensions.execute


Request Flow
============
0  app/bootstrap.php - error_reporting, autoloading composer+own, functions, timezone
1. create bootstrap - object manager factory, root, params
2. bootstrap.createApplication - new object manager, new app/http
3. bootstrap.run(app)
  - error handler
  - maintenance
  - instaled
app.launch:
- object manager.configure (area by front name)
- front controller.dispatch
  router list - adminhtml:
  -  10 admin (Magento\Backend\App\Router, module-backend)
  - 100 default (Magento\Framework\App\Router\DefaultRouter, module-backend)
  router list - frontend:
  -  20 standard (Magento\Framework\App\Router\Base, module-store)
  -  20 urlrewrite (Magento\UrlRewrite\Controller\Router, module-url-rewrite)
  -  60 cms (Magento\Cms\Controller\Router, module-cms)
  - 100 default (Magento\Framework\App\Router\DefaultRouter, module-store)

## Constants:
BP - /
## App types:
http, cron (cron.php), media (get.php), static resource (static.php), indexer?, user config?


Playground
==========
    require __DIR__ . '/app/bootstrap.php';

    $bootstrap = Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();

    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('frontend');

    // ...

JS Sranslations
===============
js-translation.json
\Magento\Deploy\Model\Deploy\LocaleDeploy::deploy $this->deployFile($dictionaryFileName, $area, $themePath, $locale, null);
\Magento\Translation\Model\Js\PreProcessor::process
\Magento\Deploy\Model\Deploy\LocaleDeploy::deployFile


\Magento\Translation\Model\Js\DataProvider::getPhrases

HTML
  'i18n_translation' => '~i18n\\:\\s*(["\'])(.*?)(?<!\\\\)\\1~',
  'translate_wrapping' => '~translate\\=("\')([^\\\'].*?)\\\'\\"~',
JS
  'mage_translation_widget' => '~\\$\\.mage\\.__\\((?s)[^\'"]*?([\'"])(.+?)\\1(?s).*?\\)~',
  'mage_translation_static' => '~\\$t\\((?s)[^\'"]*?(["\'])(.+?)\\1(?s).*?\\)~',

    <div class="hosted-error"><!-- ko i18n: 'Please, enter valid Credit Card Number'--><!-- /ko --></div>
    <span class="label" translate="'User Agent Rules'"></span>
    'mage/translate': $.mage.__('Close')
    'mage/translate': $t('Select Date')


Knockout Integration
====================
Magento_Ui/js/lib/knockout/bootstrap
templates can be loaded from remote -- reuse templates
render page vis knockout without a view model (data)
custom bindings 'scope' will assign local view models

<pre data-bind="text: ko.toJSON($data, null, 2)"></pre>

ko.extenders.myExtender = function(target, someParameter) {
  // do something with target observable
  return target;
};
ko.observable('something').extend({myExtender: 'some parameter'});

ko.extenders.disposableCustomerData:
- after 3 seconds, remove from storage and cookie section_data_ids somehow
- e.g. {"cart":1484591441}


Ui Components
=============
app -> layout()
run
iterator
process

- addChild - current node to parent
- manipulate - appendTo, prependTo, insertTo
- initComponent
    - loadDeps
    - loadSource (source='uiComponent')
    - initComponent --- long async here --> global function var component = new Constr(_.omit(node, 'children'));


Convert_Checkout/js/view/payment: iterator, process, initComponent, loadSource, ...wait
uiComponent ...
Mageyto_Payment/js/view/payment/payments


checkout; steps; billing-step; payment; renders; free-payments; authorizenet
afterMethods; discount; additional-payment-validators .....

first loaded - initComponent - address-list-additional-addresses
initialize
-_super
-initObservable
-initModules
-initStatefull
-initLinks
-initUnique

## Text/x-magento-init
@see http://alanstorm.com/magento_2_javascript_init_scripts

- lib/web/mage/apply/scripts.js parses init scripts
- we return component-function
- magento gets it via require and invokes with arguments. E.g. uiComponent(config, node)
- node '*' -> false
- selector matching multiple nodes - multiple module instances (like jQuery plugin)
- top-level selector outside js module
- initial values from server are outside modules

<script type="text/x-magento-init">
        {
            "*": {
                "Magento_Ui/js/core/app": {
                    "components": {
                        "customer": {
                            "component": "Magento_Customer/js/view/customer"
                        }
                    },
                    "types": ...
                }
            },
            ".dom-sele-ctor": {
                "com-ponent": {
                   mixins: [],
                   option1: value1,
                   option2: value2
                }
            }
        }
</script>

## Data-mage-init
<div data-mage-init='{"path/to/component": {"con": "fig"}}'></div>

lib/web/mage/apply/main.js

    require(['path/to/component'], function(component) {
      // require mixins if present

      component({con:fig});
    })


<script type="text/x-magento-init">
    {
        "[data-gallery-role=gallery-placeholder]": {
            "mage/gallery/gallery": {
                "mixins":["magnifier/magnify"],
                "magnifierOpts": ...,
                "data": ...,
                "options": ...
            }
        }
    }
</script>

element = $('[data-gallery-role=gallery-placeholder]');
config = {magnifierOpts:..., data:..., options:...}

require(['mage/gallery/gallery'], function(gallery) {
  require(["magnifier/magnify"], function(magnify) {
    var result = magnify(config, element); // call for each mixin. Magnify will use config.magnifierOpts
    extend(config, result); // magnify returns same config, config not changed
  })
  gallery(config, element);
});


uiComponent = Magento_Ui/js/lib/core/collection = lib/core/element/element + lib/core/class

- Magento_Ui/js/core/renderer/layout(nodes, parent, cached, merge)(components)
  - component, config, children
  - config.deps, config.provider, parent, parentName, nodeTemplate, childTemplate, name, extendProvider, provider, deps, type, componentType, isTemplate

- Magento_Ui/js/core/renderer/types.set(types)
  - node.type > merge defaults from types


### regions
js/lib/core/collection: 'displayArea' -> 
_updateCollection ->

## Knockout
ko.bindingHandlers.customBinding = {
    init: ...
    upudate: ...

data-bind="scope: component"
}


context = Magento\Framework\View\Element\UiComponent\Context
$data = [
  config => [
    value = ''
  ],
  js_config => [
    extends => '',
    provider => '',
  ],
  template => '',
  html_blocks => '',
  buttons => '',
  actions => '',
  observers => ''
]

dataProvider = Magento\Ui\DataProvider\AbstractDataProvider

## Listing
template view/base/ui_component/templates/listing/default.xhtml

view layout generators:
- container
- head
- body
- block
- uiComponent Magento\Framework\View\Layout\Generator\UiComponent::process

for each <uiComponent> in layout:
- creates component by name, e.g. customer_listing
- prepares it (recursive prepares children)
- generats regular block-wrapper Magento\Ui\Component\Wrapper\UiComponent instanceof Magento\Framework\View\Element\Template
with data = prepared component (e.g. Magento\Ui\Component\Listing)
- renders block -> component->render()
- Magento\Ui\TemplateEngine\Xhtml\Result::__toString
- server-side processes template
- appendLayoutConfiguration = Magento\Framework\View\Layout\Generic.build(): [
    types => /*components definitions*/,
    components => [
       {namespace} => [
         children => children+datasources
       ]
    ]
  ]

<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
-->
<div
    class="admin__data-grid-outer-wrap"
    data-bind="scope: '{{getName()}}.{{getName()}}'"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:noNamespaceSchemaLocation="../../../../../../Ui/etc/ui_template.xsd">
    <div data-role="spinner" data-component="{{getName()}}.{{getName()}}.{{spinner}}" class="admin__data-grid-loading-mask">
        <div class="spinner">
            <span/><span/><span/><span/><span/><span/><span/><span/>
        </div>
    </div>
    <!-- ko template: getTemplate() --><!-- /ko -->
</div>
