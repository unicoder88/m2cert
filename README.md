# Magento 2 Certified Professional Developer #

+ 60 Multiple Choice items
+ 90 minutes to complete the exam
+ Magento Open Source (2.2) and Magento Commerce (2.2)
+ All exams are administered by Kryterion - Academy of Networking LANIT

# Links #

+ [Magento 2 Certified Professional Developer](https://u.magento.com/magento-2-certified-professional-developer)
+ [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
+ [Markdown Cheatsheet RU](https://github.com/sandino/Markdown-Cheatsheet)
+ [Study Guide](https://bitbucket.org/unicoder88/m2cert/raw/master/guide.pdf)
+ [Misc M2 notes]((https://bitbucket.org/unicoder88/m2cert/raw/master/misc.md))

# Topics #

+ 18% 1 [Magento Architecture and Customization Techniques](https://bitbucket.org/unicoder88/m2cert/src/master/1-Magento-Architecture-and-Customization-Techniques.md)   (11 questions)
      - modules, config, di, plugins, events, cron, cli, cache
+ 12% 2-[Request Flow Processing](https://bitbucket.org/unicoder88/m2cert/src/master/2-Request-Flow-Processing.md)                            (7 questions)
      - modes, front contr., url, rewrite, action contr., response, routes, 404, layout, page yout
+ 10% 3-Customizing the Magento UI                         (6 questions)
      - theme, template, block, block cache, JS, UI component (briefly)
+  7% 4-Working with Databases in Magento                  (4 questions)
      - repository, api data class, search criteria, table, load/save, collection, select, gration 
+  8% 5-Using the Entity-Attribute-Value (EAV) Model       (5 questions)
      - hierarchy, storage, load/save, attributes, frontend/souce/backend
+ 10% 6-Developing with Adminhtml                          (6 questions)
      - form, grid, system.xml, menu, acl
+ 12% 7-Customizing the Catalog                            (7 questions)
      - product types, price, price render, category, catalog rules
+ 13% 8-Customizing the Checkout Process                   (8 questions)
      - cart rule, add to cart*, quote totals, product type render, shipping method, payment thod
      * normal/wishlist/reorder/quote merge
+  5% 9-Sales Operations                                   (3 questions)
      - order processing, status, invoice, refund
+  5% 10-Customer Management                               (3 questions)
      - my account, customer extension attributes, address, customer group, tax
