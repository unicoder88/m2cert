# 2 Request Flow Processing

## 2.1 Utilize modes and application initialization

### Identify the steps for application initialization.
app/bootstrap:

- composer autoloader, functions, umask, timezone UTC, php precision

\Magento\Framework\App\Bootstrap::*create*

- configure autoloader - PSR-4 prepend generation\Magento

\Magento\Framework\App\Bootstrap::*createApplication*

- just call object manager->create

\Magento\Framework\App\Bootstrap::*run*

- set error handler
- assert maintenance
- assert installed
- response = application->*launch*()
- response->*sendResponse*()
- on error: application->catchException
- on missed error:
    * dev mode: print exception trace
    * normal mode: log, message "An error has happened during application run. See exception log for details."
    
#### Application architecture - main classes
1. *application class* for bootstrap->createApplication()

  - \Magento\Framework\App\Http - index.php, pub/index.php

    load config area by front name
    front controller->dispatch
    event `controller_front_send_response_before`

  - \Magento\Framework\App\Cron - pub/cron.php

    config area `crontab`
    load translations
    dispatch event `default`

  - \Magento\MediaStorage\App\Media - pub/get.php

    access /media/* when using DB image storage and physical file doesn't exist

  - \Magento\Framework\App\StaticResource - pub/static.php

    404 in production
    /$area/$resource/$file/... params, load config by params
    sends file in response
    assetRepo->createAsset - \Magento\Framework\View\Asset\File
    assetPublisher->publish - materialize (copy/symlink) file if doesn't exist

  - \Magento\Indexer\App\Indexer - module-indexer, unused?
  - \Magento\Backend\App\UserConfig - module-backend, unused?

Responsibility - launch() and return response.
Roughly different application class matches different physical file entry points (index.php, media.php, get.php, static.php, cron.php).
Front controller exists only in Http application.
There's no CLI application, Symfony command is used.

2. *Front controller* exists only in Http application (pub/index.php)

- Same entry point, how to execute different logic?
  Via different DI preference depending on detected config area (areaList->getCodeByFrontName)

- *Default* global preference app/etc/di.xml - Magento\Framework\App\FrontController
- "frontend", "adminhtml", "crontab" area code - no preference, use default *App\FrontController*
- "webapi_rest (frontName `/rest`) - preference module-webapi/etc/webapi_rest/di.xml - *\Magento\Webapi\Controller\Rest*
- "webapi_soap" (frontname `/soap`) - preference module-webapi/etc/webapi_rest/di.xml - *\Magento\Webapi\Controller\Soap*


#### HTTP application
\Magento\Framework\App\Http::launch

1. detect config area by front name

  `\Magento\Framework\App\AreaList` - areas from argument di.xml

  - frontend = [frontname null, router "standard"] --- *default when nothing matched*
  - adminhtml - [frontNameResolver=..., router "admin"]
    \Magento\Backend\App\Area\FrontNameResolver::getFrontName(checkhost)
    system config `admin/url/use_custom`, `admin/url/custom`
  - crontab = null
  - webapi_rest = [frontName `/rest`]
  - webapi_soap = [frontname `/soap`]

1. ObjectManagerInterface::configure - selected area code
1. result = FrontControllerInterface->dispatch
1. ResultInterface.renderResult into response object
1. event `controller_front_send_response_before` (request, response)

### Describe front controller responsibilities

*Front controller* exists only in Http application (pub/index.php)

- Same entry point, how to execute different logic?
  Via different DI preference depending on detected config area (areaList->getCodeByFrontName)
- *Default* global preference app/etc/di.xml - Magento\Framework\App\FrontController
- "frontend", "adminhtml", "crontab" area code - no preference, use default *App\FrontController*
- "webapi_rest (frontName `/rest`) - preference module-webapi/etc/webapi_rest/di.xml - *\Magento\Webapi\Controller\Rest*
- "webapi_soap" (frontname `/soap`) - preference module-webapi/etc/webapi_rest/di.xml - *\Magento\Webapi\Controller\Soap*

#### App\FrontController:

- routerList
- action = router[].match
- result = action.dispatch() or action.execute()
- noroute action fallback

#### Router match - action can be:

- generic \Magento\Framework\App\ActionInterface::execute - not used?
- \Magento\Framework\App\Action\AbstractAction::dispatch - context, request, response, result factory, result redirect factory

#### Dispatch/execute action - result can be:

- \Magento\Framework\Controller\ResultInterface - renderResult, setHttpResponseCode, setHeader

  Raw, Json, Forward, Layout, Page, Redirect

- \Magento\Framework\App\ResponseInterface - sendResponse

  Console\Response, Response\FileInterface, PhpEnvironment\Response, Webapi\Response, Rest\Response
